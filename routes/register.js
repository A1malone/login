const express = require("express");
const router = express.Router();
const User = require("../models/user");
const bcrypt = require("bcryptjs");
const dotenv = require("dotenv");

dotenv.config({
  path: "../config.env"
});

//@route    Post api/user
//@desc     Register user
//@access   Public

router.post("/register", async (req, res) => {
  try {
    //encrypt password
    const salt = await bcrypt.genSalt(10);

    const hashed = await bcrypt.hash(req.body.password, salt);

    const user = {
      name: req.body.first_name + " " + req.body.last_name,
      password: hashed,
      email: req.body.email,
      date: Date.now()
    };
    let newUser = new User(user);

    //saves user in database
    await newUser.save().then(res.redirect("/"));
  } catch (err) {
    console.error(`Registration Error: ${err.message}`);
    res.status(500).send(`Registration Error: ${err.message}`);
  }
});

module.exports = router;
