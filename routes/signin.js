const express = require("express");
const router = express.Router();
const User = require("../models/user");
const bcrypt = require("bcryptjs");
const dotenv = require("dotenv");

dotenv.config({
  path: "../config.env"
});

//@route    POST user
//@desc     Login
//@access   Public

router.post("/SignIn", async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.username });

    if (user.email == null) {
      return res.status(400).send("cant find user");
    }
    if (await bcrypt.compare(req.body.login_password, user.password)) {
      res.send("success for now");
    } else {
      res.send("invalid password");
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).send(`Login Error: ${err.message}`);
  }
});

module.exports = router;
