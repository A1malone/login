const express = require("express");
const app = express();
const path = require("path");
const dotenv = require("dotenv");
const connectDB = require("./public/db");
const bodyParser = require("body-parser");

app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

app.use(bodyParser.json());

//Load env
dotenv.config({
  path: "./config.env"
});

//Connect to MongoDB
connectDB();

// app.use + app.get below make the homepage work
app.use(express.static(path.join(__dirname, "public")));
app.get("/", (req, res) =>
  res.sendFile(path.join(__dirname + "/views/index.html"))
);

// Routes
app.use("/login", require("./routes/register"));
app.use("/login", require("./routes/signin"));

const port = process.env.PORT || 8000;

app.listen(port, () => {
  console.log(
    `Server running in ${process.env.NODE_ENV} mode and on port ${port}`
  );
});
